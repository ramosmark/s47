const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFirstName = document.querySelector('#span-full-name1')
const spanLastName = document.querySelector('#span-full-name2')

txtFirstName.addEventListener('input', (e) => {
	spanFirstName.innerHTML = txtFirstName.value
})

txtLastName.addEventListener('input', (e) => {
	spanLastName.innerHTML = txtLastName.value
})
